//
//  Employee.h
//  Sample1
//
//  Created by Ankur on 09/01/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "Person.h"

@interface Employee : Person
{
    NSString *employeeEducation;
}

- (id)initWithName:(NSString *)name andAge:(NSInteger)age
      andEducation:(NSString *)education;
- (void)print;

@end
