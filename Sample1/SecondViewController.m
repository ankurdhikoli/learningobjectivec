//
//  SecondViewController.m
//  Sample1
//
//  Created by Ankur on 20/10/15.
//  Copyright © 2015 your company. All rights reserved.
//

#import "SecondViewController.h"

@implementation SecondViewController


- (void)viewDidLoad {
    [super viewDidLoad];
   

    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self gcdTypesDiffernce];
    [self differrenceBetweenCopyAnyMutableCopy];

}


-(void) gcdTypesDiffernce
{
    
    /*
     //Do something
     dispatch_async(queue, ^{
     //Do something else
     });
     //Do More Stuff
     
     
     ///Do something
     dispatch_sync(queue, ^{
     //Do something else
     });
     //Do More Stuff
     
     */
    
    // http://stackoverflow.com/questions/19822700/difference-between-dispatch-async-and-dispatch-sync-on-serial-queue
    
    dispatch_queue_t _serialQueue = dispatch_queue_create("com.example.name", DISPATCH_QUEUE_SERIAL);
    
    dispatch_async(_serialQueue, ^{
        printf("1");
    });
    printf("2");
    dispatch_async(_serialQueue, ^{
        printf("3");
    });
    printf("4");
    
    
    printf("\n");
    
    
    dispatch_sync(_serialQueue, ^{
        printf("1");
    });
    printf("2");
    
    // it will not update the UI button will hang activator will spin
    dispatch_sync(_serialQueue, ^{
        int i;
        for (i=0; i<100; i++) {
            // _progress.progress=i*1.0/100000;
            //  printf("3 %f",_progress.progress);
            printf("3 ");
            
        }
    });
    
    printf("4");
    
}

-(void) differrenceBetweenCopyAnyMutableCopy {

    NSString *string1 = @"string1";
    NSString *string2 = string1;
    
    NSLog(@"Test 1(%p, %p): String 1: %@; String 2: %@", string1, string2, string1, string2);
    
    string1 = nil;
    
    NSLog(@"Test 2(%p, %p): String 1: %@; String 2: %@", string1, string2, string1, string2);
    
    string1 = string2;
    
    NSLog(@"Test 3(%p, %p): String 1: %@; String 2: %@", string1, string2, string1, string2);
    
    string2 = [string1 mutableCopy];
    
    NSLog(@"Test 4(%p, %p): String 1: %@; String 2: %@", string1, string2, string1, string2);
    
    string2 = [string1 copy];
    
    NSLog(@"Test 5(%p, %p): String 1: %@; String 2: %@", string1, string2, string1, string2);
    
    
    NSDictionary *dict =[NSDictionary dictionaryWithObjectsAndKeys:
                         @"ank",@"key1",
                         @"ank",@"key2",
                         @"ank",@"key3",
                         @"ank",@"key4",
                         @"ank",@"key5",
                         nil];
    NSDictionary *dict2 =[NSDictionary dictionaryWithObjectsAndKeys:
                          @"ank",@"key1",
                          @"ank",@"key2",
                          @"ank",@"key3",
                          @"ank",@"key4",
                          @"ank",@"key5",
                          nil];
    
    NSDictionary *dict3 =[NSDictionary dictionaryWithObjectsAndKeys:
                          @"ank",@"key1",
                          @"ank",@"key2",
                          @"ank",@"key3",
                          @"ank",@"key4",
                          @"ank",@"key5",
                          nil];
    
    
    NSArray *arrayone =[ NSArray arrayWithObjects:dict, dict2,dict3, nil];
    
    NSMutableArray *arraymut = [arrayone mutableCopy];
    
    NSArray *arrayCopy = [arrayone copy];
    
    NSLog(@"nsarray%p arraymut%p arraycopy%p",arrayone,arraymut,arrayCopy);
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
