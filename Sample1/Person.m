//
//  Person.m
//  Sample1
//
//  Created by Ankur on 09/01/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "Person.h"

@implementation Person

- (id)initWithName:(NSString *)name andAge:(NSInteger)age{
    personName = name;
    personAge = age;
    return self;
}


- (void)print{
    NSLog(@"Name: %@", personName);
    NSLog(@"Age: %ld", personAge);
}


@end
