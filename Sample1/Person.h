//
//  Person.h
//  Sample1
//
//  Created by Ankur on 09/01/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
{

    NSString *personName;
    NSInteger personAge;
}

- (id)initWithName:(NSString *)name andAge:(NSInteger)age;
- (void)print;


@end
