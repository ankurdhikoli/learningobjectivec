//
//  CategoriesViewController.h
//  Sample1
//
//  Created by Ankur on 23/02/17.
//  Copyright © 2017 your company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesViewController : UIViewController
-(void) somePublicMethod;
@property(readonly, copy) NSString *firstname;
@property(readonly, nonatomic) NSString *lastName;

@end
