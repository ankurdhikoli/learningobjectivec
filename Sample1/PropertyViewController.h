//
//  PropertyViewController.h
//  Sample1
//
//  Created by Ankur on 23/02/17.
//  Copyright © 2017 your company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PropertyViewController : UIViewController

@property (nonatomic, assign) BOOL sampleBool;

// Atomic by default and readwrite
@property NSNumber *yearOfBirth;
@property int yearOfBirthSecond;

 @property (readonly) NSString *firstName;


@end
