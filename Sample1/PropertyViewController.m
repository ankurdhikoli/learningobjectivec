//
//  PropertyViewController.m
//  Sample1
//
//  Created by Ankur on 23/02/17.
//  Copyright © 2017 your company. All rights reserved.
//

// readwrite is the default behaviour
// strong is the default behaviour
//http://stackoverflow.com/questions/17721724/assigning-readonly-property

// fo rread only property So you should directly set the ivar, and this can be done only from inside the object for which readonly property is defined.

#import "PropertyViewController.h"
#import "AnyItem.h"

@interface PropertyViewController ()
//@property (nonatomic, copy) NSString *name;

@end

@implementation PropertyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //string Property retain or copy
    //http://stackoverflow.com/questions/387959/nsstring-property-copy-or-retain
   //http://stackoverflow.com/questions/4995254/nsmutablestring-as-retain-copy/5002646#5002646
// copy property return same value and retain property return updated value with a increase in reatin count
    AnyItem *item = [[AnyItem alloc] init];
    NSMutableString *someitemName = [NSMutableString stringWithString:@"Chris"];
    NSMutableString *someSecondItemName = [NSMutableString stringWithString:@"ankur"];
    
    item.itemName = someitemName;
    item.seconditemName = someSecondItemName;

    [someitemName setString:@"Debajit"];
    [someSecondItemName setString:@"Chauhan"];

    NSLog(@"seconditem=%@ itemName=%@", item.seconditemName, item.itemName);

//log
 //  seconditem=Chauhan itemName=Chris
    
    // this will give a error beacuse we are setting readonly property
    //self.firstName = @"ankur";
    
    // try this way for ready only property and this cant be set out from another class
    _firstName =@"ankur";
    
    // Do any additional setup after loading the view.
    
    /*
     
     is it OK to assign to a property that is readonly?
     
     Yes, its okay if you don't want a property to be mutated outside its containing instance.
     
     eg: count property of an array, here count is a property that is dependent on the number of objects retained by the array. So it shouldn't be modified from outside the Array object.
     should not coordinate have self before it? (e.g., self.coordinate = c).
     
     If read only, you can't modify your property via a setter method.
     
     self.coordinate translates to [self setCoordinate:] which is not permitted as read only prevents setter methods from modifying the property.
     So you should directly set the ivar, and this can be done only from inside the object for which readonly property is defined.
     
     i.e
     
     If you are curious,
     
     self.propertyName && self.property = indirectly calls getter and setter method.
     And in implementation, setters and getter help the _ivars to be public-ally accessible is the property is public.
     
     - (void)setProperty(value) //self.property = value
     {
     _property = value;
     }
     
     - (id)property  //self.property
     {
     return _property;
     }
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
