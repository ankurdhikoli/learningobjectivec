//
//  WhoCallFirstViewController.m
//  Sample1
//
//  Created by Ankur on 9/25/16.
//  Copyright © 2016 your company. All rights reserved.
//

#import "WhoCallFirstViewController.h"

@interface WhoCallFirstViewController ()

@end

@implementation WhoCallFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewDidLoad %@",timestamp);

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Do any additional setup after loading the view.
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewDidAppear %@",timestamp);

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view.
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewWillAppear %@",timestamp);

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewWillDisappear %@",timestamp);

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewDidDisappear %@",timestamp);
}

- (void)viewWillLayoutSubviews{
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewWillLayoutSubviews %@",timestamp);

}
- (void)viewDidLayoutSubviews {
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSLog(@"viewDidLayoutSubviews %@",timestamp);

};

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 2016-09-25 21:20:42.571 Sample1[8440:886837] viewWillAppear 1474856442571.094971
 2016-09-25 21:20:42.574 Sample1[8440:886837] viewWillLayoutSubviews 1474856442573.978027
 2016-09-25 21:20:42.574 Sample1[8440:886837] viewDidLayoutSubviews 1474856442574.273193
 2016-09-25 21:20:43.076 Sample1[8440:886837] viewDidAppear 1474856443076.016846
 2016-09-25 21:20:43.076 Sample1[8440:886837] viewWillLayoutSubviews 1474856443076.785156
 2016-09-25 21:20:43.077 Sample1[8440:886837] viewDidLayoutSubviews 1474856443077.070068
 */

@end
