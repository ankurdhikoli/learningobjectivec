//
//  Employee.m
//  Sample1
//
//  Created by Ankur on 09/01/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "Employee.h"

@implementation Employee

- (id)initWithName:(NSString *)name andAge:(NSInteger)age
      andEducation: (NSString *)education
{
    personName = name;
    personAge = age;
    employeeEducation = education;
    return self;
}

- (void)print
{
    NSLog(@"Name: %@", personName);
    NSLog(@"Age: %ld", personAge);
    NSLog(@"Education: %@", employeeEducation);
}


@end
