//
//  ViewController.m
//  Sample1
//
//  Created by Ankur on 09/01/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

// Topics Covering
// dynamic typing and binding
// polymorphism
// oops concepts
// inheritance
//Try,Catch,Finally
// Exceptions
// abstract class and concerete class
// comparing two objects

#import "ViewController.h"
#import "Person.h"
#import "Employee.h"
#import "CategoriesViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //CategoriesViewController * cat = [[CategoriesViewController alloc] init];
    //cat.firstname = @"ankur";
//    [cat somePublicMethod];
    // notice somePrivateMethod is not accesible  due to class extension
    
    // Do any additional setup after loading the view, typically from a nib.
    
    // http://www.slideshare.net/tiyasi/oops-features-using-objective-c
    // inheritance
    //  @interface derived-class: base-class
    // proces by which one object acquires functionality of another object
    // super class 4 wheelers -> sub class truck,bus,car
    Person *person = [[Person alloc]initWithName:@"Raj" andAge:5];
    [person print];
    NSLog(@"Inherited Class Employee Object");
    Employee *employee = [[Employee alloc]initWithName:@"Raj"
                                                andAge:5 andEducation:@"MBA"];
    [employee print];
    
   // dynamic Typing
    // http://www.slideshare.net/AtitPatumvan/chapter-9-polymorphism-dynamic-typing-and-dynamic-binding?next_slideshow=2
    // Dynamic typing in Objective-C means that the class of an object of type id is unknown at compile time, and instead is discovered at runtime when a message is sent to the object.
    NSArray *array=[[NSArray alloc]init]; // this can be anything or ay object which we are getting from any where
    id dynamicObject = array;
    BOOL isArray= [dynamicObject isKindOfClass:[NSArray class]];
    NSLog(@"its array if its returning One = %d",isArray);
    
    // Dynamic binding
    // http://www.tutorialspoint.com/objective_c/objective_c_dynamic_binding.htm
    //means that the compiler doesn't know which method implementation will be selected; instead the method implementation is looked up at runtime when the message is sent. So
   // [foo anymethodName]
   //  results in invoking a different method implementation if, for example, foo is an instance of NSArray rather than an instance of NSString.
    
    
    // polymorphism
    // same name different classes

    // [self applyWidget:nil];
    //[self applyWidget:@"ankur"];
    //[self applyWidget:2356.38];
   // - (void)applyWidget: (id)widget;
  //  Then pass either an NSString or an NSNumber wrapping your double. Then, in the implementation of the method, use Objective-C's introspection methods to determine how to proceed:
    
    // data encapsulation
//    http://www.tutorialspoint.com/objective_c/objective_c_data_encapsulation.htm
    
    
    // Abstract and Concerete class
  //  A concrete class is one that is actually used "as is" for some purpose. A abstract class is a class that is subclassed but has little functionality on it's own. Example NSObject is a abstract class(never use it as is). UIActivityIndicator is a concrete class(pretty much always use it as is).
    
    
    // Handling  Exceptions
    
    
    NSArray *inventory = @[@"Honda Civic",
                           @"Nissan Versa",
                           @"Ford F-150"];
    int selectedIndex = 3;
    @try {
        NSString *car = inventory[selectedIndex];
        NSLog(@"The selected car is: %@", car);
    } @catch(NSException *theException) {
        NSLog(@"An exception occurred: %@", theException.name);
        NSLog(@"Here are some details: %@", theException.reason);
        //NSLog(@"Here are some userInfo : %@", theException.userInfo);
        
    } @finally {
        NSLog(@"Executing finally block");
    }
    
    
    
    // Handling Error
    
    NSString *fileToLoad = @"/path/to/non-existent-file.txt";
    NSError *error;
    NSString *content = [NSString stringWithContentsOfFile:fileToLoad
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];
    
    if (content == nil) {
        // Method failed
        NSLog(@"Error loading file %@!", fileToLoad);
        NSLog(@"Domain: %@", error.domain);
        NSLog(@"Error Code: %ld", (long)error.code);
        NSLog(@"Description: %@", [error localizedDescription]);
        NSLog(@"Reason: %@", [error localizedFailureReason]);
    } else {
        // Method succeeded
        NSLog(@"Content loaded!");
        NSLog(@"%@", content);
    }

    
    
    
    // comparing two objects
    NSString *aString = [NSString stringWithFormat:@"Hello"];
    NSString *bString = aString;
    NSString *cString = [NSString stringWithFormat:@"Hello"];
    
    if (aString == bString)
        NSLog(@"CHECK 1");
    
    if (bString == cString)
        NSLog(@"CHECK 2");
    
    if ([aString isEqual:bString])
        NSLog(@"CHECK 3");
    
    if ([bString isEqual:cString])
        NSLog(@"CHECK 4");
    
    // Look at the pointers:
    NSLog(@"%p", aString);
    NSLog(@"%p", bString);
    NSLog(@"%p", cString);
    
    
}


- (void)applyWidget: (id)widget{
// Then pass either an NSString or an NSNumber wrapping your double. Then, in the implementation of the method, use Objective-C's introspection methods to determine how to proceed:
if ([widget isKindOfClass: [NSString class]]) {
    NSLog(@"%@",widget);
} else if ([widget isKindOfClass: [NSNumber class]]) {
    double d = [widget doubleValue];
    NSLog(@"%f",d);
}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
