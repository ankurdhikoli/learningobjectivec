//
//  AnyItem.h
//  Sample1
//
//  Created by Ankur on 23/02/17.
//  Copyright © 2017 your company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnyItem : NSObject
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, retain) NSString *seconditemName;

@end
