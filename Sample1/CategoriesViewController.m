//
//  CategoriesViewController.m
//  Sample1
//
//  Created by Ankur on 23/02/17.
//  Copyright © 2017 your company. All rights reserved.
//

//http://stackoverflow.com/questions/172598/best-way-to-define-private-methods-for-a-class-in-objective-c
// no such thing called private methods in objective-c use category for this
// a category with an empty name (i.e. @interface MyClass ()) called Class Extension
// Class(WithEmpthInside) is called Anynomous category or class extension

#import "CategoriesViewController.h"

@interface CategoriesViewController ()
{
}
-(void) somePrivateMethod;

@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _firstname = @"ankur";
    _lastName = @"chauhan";
   // self.lastName = @"chauhan";

    // Do any additional setup after loading the view.
}

-(void) somePublicMethod
{
    NSLog(@"Hello i am public");
}

-(void) somePrivateMethod
{
    NSLog(@"Hello i am private");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
